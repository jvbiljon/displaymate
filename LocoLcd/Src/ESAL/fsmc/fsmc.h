#ifndef __FSMC_H
#define __FSMC_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "main.h"

void FSMC_vInit(void);
void HAL_NOR_MspInit(NOR_HandleTypeDef* hnor);
void HAL_NOR_MspDeInit(NOR_HandleTypeDef* hnor);
void HAL_SRAM_MspInit(SRAM_HandleTypeDef* hsram);
void HAL_SRAM_MspDeInit(SRAM_HandleTypeDef* hsram);

#ifdef __cplusplus
}
#endif
#endif /*__FSMC_H */

