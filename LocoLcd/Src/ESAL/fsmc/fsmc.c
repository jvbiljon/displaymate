#include "fsmc.h"
#include "Src/ESAL/init/init.h"

NOR_HandleTypeDef hnor1;
SRAM_HandleTypeDef hsram2;

static uint32_t FSMC_Initialized = 0;

void FSMC_vInit(void)
{
	FSMC_NORSRAM_TimingTypeDef Timing;

	/* Perform the NOR1 memory initialization sequence */
	hnor1.Instance = FSMC_NORSRAM_DEVICE;
	hnor1.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
	/* hnor1.Init */
	hnor1.Init.NSBank = FSMC_NORSRAM_BANK1;
	hnor1.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
	hnor1.Init.MemoryType = FSMC_MEMORY_TYPE_NOR;
	hnor1.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
	hnor1.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
	hnor1.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
	hnor1.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
	hnor1.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
	hnor1.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
	hnor1.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
	hnor1.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
	hnor1.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
	hnor1.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
	hnor1.Init.PageSize = FSMC_PAGE_SIZE_NONE;
	/* Timing */
	Timing.AddressSetupTime = 5;
	Timing.AddressHoldTime = 1;
	Timing.DataSetupTime = 5;
	Timing.BusTurnAroundDuration = 1;
	Timing.CLKDivision = 0;
	Timing.DataLatency = 1;
	Timing.AccessMode = FSMC_ACCESS_MODE_B;
	/* ExtTiming */

	if (HAL_NOR_Init(&hnor1, &Timing, NULL) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/** Perform the SRAM2 memory initialization sequence
	*/
	hsram2.Instance = FSMC_NORSRAM_DEVICE;
	hsram2.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
	/* hsram2.Init */
	hsram2.Init.NSBank = FSMC_NORSRAM_BANK3;
	hsram2.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
	hsram2.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
	hsram2.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
	hsram2.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
	hsram2.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
	hsram2.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
	hsram2.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
	hsram2.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
	hsram2.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
	hsram2.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
	hsram2.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
	hsram2.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
	hsram2.Init.PageSize = FSMC_PAGE_SIZE_NONE;
	/* Timing */
	Timing.AddressSetupTime = 1;
	Timing.AddressHoldTime = 0;
	Timing.DataSetupTime = 4;
	Timing.BusTurnAroundDuration = 0;
	Timing.CLKDivision = 0;
	Timing.DataLatency = 0;
	Timing.AccessMode = FSMC_ACCESS_MODE_A;
	/* ExtTiming */

	if (HAL_SRAM_Init(&hsram2, &Timing, NULL) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}

static void HAL_FSMC_MspInit(void) 
{
	if (FSMC_Initialized) 
	{
		return;
	}
	FSMC_Initialized = 1;
	/* Peripheral clock enable */
	__HAL_RCC_FSMC_CLK_ENABLE();
  
	/* FSMC GPIO Configuration */
	GPIO_InitStructure.Pin = GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 
	                        | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 
	                        | GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 
	                        | GPIO_PIN_15;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF12_FSMC;

	HAL_GPIO_Init(GPIOE, &GPIO_InitStructure);

	/* GPIO_InitStructure */
	GPIO_InitStructure.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 
	                        | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_12 | GPIO_PIN_13 
	                        | GPIO_PIN_14 | GPIO_PIN_15;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF12_FSMC;

	HAL_GPIO_Init(GPIOF, &GPIO_InitStructure);

	/* GPIO_InitStructure */
	GPIO_InitStructure.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 
	                        | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_10;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF12_FSMC;

	HAL_GPIO_Init(GPIOG, &GPIO_InitStructure);

	/* GPIO_InitStructure */
	GPIO_InitStructure.Pin = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 
	                        | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15 
	                        | GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_4 | GPIO_PIN_5 
	                        | GPIO_PIN_6 | GPIO_PIN_7;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF12_FSMC;

	HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);
}

void HAL_NOR_MspInit(NOR_HandleTypeDef* norHandle) 
{
	HAL_FSMC_MspInit();
}

void HAL_SRAM_MspInit(SRAM_HandleTypeDef* sramHandle) 
{
	HAL_FSMC_MspInit();
}

static uint32_t FSMC_DeInitialized = 0;

static void HAL_FSMC_MspDeInit(void) 
{
	if (FSMC_DeInitialized) {
		return;
	}
	FSMC_DeInitialized = 1;
	/* Peripheral clock enable */
	__HAL_RCC_FSMC_CLK_DISABLE();
  
	/* FSMC GPIO Configuration  */
	HAL_GPIO_DeInit(GPIOE,
		GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6 
	                        |GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10 
	                        |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14 
	                        |GPIO_PIN_15);

	HAL_GPIO_DeInit(GPIOF,
		GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
	                        |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_12|GPIO_PIN_13 
	                        |GPIO_PIN_14|GPIO_PIN_15);

	HAL_GPIO_DeInit(GPIOG,
		GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
	                        |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_10);

	HAL_GPIO_DeInit(GPIOD,
		GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
	                        |GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15 
	                        |GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5 
	                        |GPIO_PIN_6|GPIO_PIN_7);

}

void HAL_NOR_MspDeInit(NOR_HandleTypeDef* norHandle) 
{
	HAL_FSMC_MspDeInit();
}

void HAL_SRAM_MspDeInit(SRAM_HandleTypeDef* sramHandle) 
{
	HAL_FSMC_MspDeInit();
}
