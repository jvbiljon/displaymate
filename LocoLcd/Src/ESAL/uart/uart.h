/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UART_H
#define __UART_H

#include "main.h"
#include "stm32f4xx_hal.h"

#define USART3RXBUFFERSIZE	1024
#define USART5RXBUFFERSIZE	2000

enum
{
	UART5_Receive = 0,
	UART5_Transmit
};

typedef struct
{
	uint8_t USART3RXBUFFER[USART3RXBUFFERSIZE];
	uint16_t u16Rx3WriteIndex;
	uint16_t u16Rx3ReadIndex;
} UART3_tstRxFifo;

typedef struct
{
	uint8_t USART5RXBUFFER[USART5RXBUFFERSIZE];
	uint16_t u16Rx5WriteIndex;
	uint16_t u16Rx5ReadIndex;
} UART6_tstRxFifo;

void USART3_vClearRxBuffer(void);
void USART3_vClearTxBuffer(void);
void USART3_vSend(char[], uint16_t);
void USART3_vTransmitString(uint16_t);
void USART3_vClearRxIndex(void);
void USART3_vDebugPrint(char []);

void USART5_vClearRxBuffer(void);
void USART5_vClearTxBuffer(void);
void USART5_vSend(char[]);
void USART5_vTransmitString(void);
void USART5_vClearRxIndex(void);
uint8_t UART5_u8CheckMode(void);

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *);

#endif /* __UART_H */