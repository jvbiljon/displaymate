#include "Src/ESAL/uart/uart.h"
#include "Src/ESAL/interrupts/interrupts.h"

#define USART3TXBUFFERSIZE	512
#define USART3TEMPBUFFSIZE	2

#define USART5TXBUFFERSIZE	512
#define USART5TEMPBUFFSIZE	2

uint8_t USART3TXBUFFER[USART3TXBUFFERSIZE] = "0";
uint8_t USART3TEMPBUFF[USART3TEMPBUFFSIZE];

uint8_t USART5TXBUFFER[USART5TXBUFFERSIZE] = "0";
uint8_t USART5TEMPBUFF[USART5TEMPBUFFSIZE];

UART3_tstRxFifo UART3_RxFifo;
UART6_tstRxFifo UART5_RxFifo;

uint8_t UART5_TxRxMode;
uint16_t USART5_TxBufferLen = 0;

void USART3_vClearRxBuffer(void)
{
	uint16_t i = 0;
	for (i = 0; i < USART3RXBUFFERSIZE; i++)
	{
		UART3_RxFifo.USART3RXBUFFER[i] = 0;	
	}
}

void USART3_vClearTxBuffer(void)
{
	uint16_t i = 0;
	for (i = 0; i < USART3TXBUFFERSIZE; i++)
	{
		USART3TXBUFFER[i] = 0;	
	}
}

void USART3_vSend(char data[], uint16_t len)
{
	uint16_t i = 0;
	
	/* Clear Buffer for Writing */
	USART3_vClearTxBuffer();
	/* Convert string buffer to correct format */
	for (i = 0; i <  len; i++)
	{
		USART3TXBUFFER[i] = (uint8_t) data[i]; 
	}

	/* Now send the String */
	USART3_vTransmitString(len);
}

void USART3_vTransmitString(uint16_t u16size)
{
	uint32_t u32TxDelayCounter = 0;
	
	if (u16size > 5)
	{
		u32TxDelayCounter = u16size / 5;
	}
	else
	{
		u32TxDelayCounter = 1;
	}

	HAL_UART_Transmit(&HAL_Uart3, USART3TXBUFFER, sizeof(USART3TXBUFFER), u32TxDelayCounter);	
}

void USART3_vClearRxIndex(void)
{
	UART3_RxFifo.u16Rx3WriteIndex = 0;	
}

void USART3_vDebugPrint(char data[])
{
	uint16_t u16i = 0;
	uint16_t len = 0;
	/* Get data length */
	for (u16i = 0; u16i < 1024; u16i++)
	{
		if (data[u16i] != 0)
		{
			len++;
		}
		else
		{
			break;
		}
	}
	
#ifdef DEBUG
	USART3_vSend(data, len);
#endif // DEBUG
}

void USART5_vClearRxBuffer(void)
{
	uint16_t i = 0;
	for (i = 0; i < USART5RXBUFFERSIZE; i++)
	{
		UART5_RxFifo.USART5RXBUFFER[i] = 0;	
	}
}

void USART5_vClearTxBuffer(void)
{
	uint16_t i = 0;
	for (i = 0; i < USART5TXBUFFERSIZE; i++)
	{
		USART5TXBUFFER[i] = 0;	
	}
}

void USART5_vSend(char data[])
{
	uint16_t i = 0;

	/* Set RS485 to Tx Direction */
	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_0, GPIO_PIN_SET);
	UART5_TxRxMode = UART5_Transmit;
	/* Clear Buffer for Writing */
	USART5_vClearTxBuffer();
	/* Convert string buffer to correct format */
	while (!(data[i] == 0 && data[i + 1] == 0 && data[i + 2] == 0))
	{
		USART5TXBUFFER[i] = (uint8_t) data[i]; 
		i++;
	}

	/* Save the buffer length */
	USART5_TxBufferLen = i;
	
	/* Now send the String */
	USART5_vTransmitString();
}

void USART5_vTransmitString(void)
{
	HAL_UART_Transmit_IT(&HAL_Uart5, USART5TXBUFFER, USART5_TxBufferLen);	
}

void USART5_vClearRxIndex(void)
{
	UART5_RxFifo.u16Rx5WriteIndex = 0;	
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *haurt)
{
	int u8i = 0;
	
	switch ((int)haurt->Instance)
	{
	case (int) USART3:
		{
			__HAL_UART_FLUSH_DRREGISTER(&HAL_Uart3);     // Clear the buffer to prevent overrun

			/* Add character to RX Buffer */
			UART3_RxFifo.USART3RXBUFFER[UART3_RxFifo.u16Rx3WriteIndex] = USART3TEMPBUFF[u8i];
			UART3_RxFifo.u16Rx3WriteIndex++;
			
			/* Reset counter to circular buffer */
			if (UART3_RxFifo.u16Rx3WriteIndex > USART3RXBUFFERSIZE)
			{
				UART3_RxFifo.u16Rx3WriteIndex = 0;
			}		
		}
		break;
		
	case (int) UART5:
		{
			__HAL_UART_FLUSH_DRREGISTER(&HAL_Uart5);       // Clear the buffer to prevent overrun

			/* Add character to RX Buffer */
			UART5_RxFifo.USART5RXBUFFER[UART5_RxFifo.u16Rx5WriteIndex] = USART5TEMPBUFF[u8i];
			UART5_RxFifo.u16Rx5WriteIndex++;
			/* Reset counter to circular buffer */
			if (UART5_RxFifo.u16Rx5WriteIndex >= USART5RXBUFFERSIZE)
			{
				UART5_RxFifo.u16Rx5WriteIndex = 0;
			}					
		}
		break;

	default:
		break;
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *haurt)
{
	/* Called when data transmission is complete */
	switch ((int)haurt->Instance)
	{
	case (int) UART5:
		{
			HAL_GPIO_WritePin(GPIOG, GPIO_PIN_8, GPIO_PIN_RESET);	
			UART5_TxRxMode = UART5_Receive;
		}
	default:
		break;
	}
}

uint8_t UART5_u8CheckMode(void)
{
	return UART5_TxRxMode;
}


