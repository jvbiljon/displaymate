#ifndef SSD1963_CMD_H_
#define SSD1963_CMD_H_

#include "main.h"
#include "stm32f4xx_hal.h"

// SSD1963 command table
#define CMD_NOP					0x00	//No operation
#define CMD_SOFT_RESET			0x01	//Software reset
#define CMD_GET_PWR_MODE		0x0A	//Get the current power mode
#define CMD_GET_ADDR_MODE		0x0B	//Get the frame memory to the display panel read order
#define CMD_GET_PIXEL_FORMAT	0x0C	//Get the current pixel format
#define CMD_GET_DISPLAY_MODE	0x0D	//Returns the Display Signal Mode
#define CMD_GET_SIGNAL_MODE		0x0E	//Get the current display mode from the peripheral
#define CMD_GET_DIAGNOSTIC		0x0F	//
#define CMD_ENT_SLEEP			0x10	//Turn OFF the panel
#define CMD_EXIT_SLEEP			0x11	//Turn ON the panel
#define CMD_ENT_PARTIAL_MODE	0x12	//Part of the display area is used for image display
#define CMD_ENT_NORMAL_MODE		0x13	//The whole display area is used for image display
#define CMD_EXIT_INVERT_MODE	0x20	//Displayed image colours are NOT inverted
#define CMD_ENT_INVERT_MODE		0x21	//Displayed image colours are inverted
#define CMD_SET_GAMMA			0x26	//Selects the Gamma curve used by the display device
#define CMD_BLANK_DISPLAY		0x28	//Blanks the display device
#define CMD_ON_DISPLAY			0x29	//Show the image on the display device
#define CMD_SET_COLUMN			0x2A	//Set the column extent
#define CMD_SET_PAGE			0x2B	//Set the page extent
#define CMD_WR_MEMSTART			0x2C	//Transfer image from MCU to Device starting @ CMD_SET_COLUMN & CMD_SET_PAGE
#define CMD_RD_MEMSTART			0x2E	//Transfer image from Device to MCU starting @ CMD_SET_COLUMN & CMD_SET_PAGE
#define CMD_SET_PARTIAL_AREA	0x30	//Defines the partial display area on the device
#define CMD_SET_SCROLL_AREA		0x33	//Defines the vertical scrolling and fixed area on the display area
#define CMD_SET_TEAR_OFF		0x34	//Synchronisation information is not sent from the display to the MCU
#define CMD_SET_TEAR_ON			0x35	//Synchronisation information is sent from the display to the MCU
#define CMD_SET_ADDR_MODE		0x36	//Set the read order from the frame buffer to the display panel
#define CMD_SET_SCROLL_START	0x37	//Defines the vertical scrolling start point
#define CMD_EXIT_IDLE_MODE		0x38	//Full colour depth is used for the display panel
#define CMD_ENT_IDLE_MODE		0x39	//Reduced colour depth is used for the display panel
#define CMD_SET_PIXEL_FORMAT	0x3A	//Defines how many bits per pixel is used
#define CMD_WR_MEM_AUTO			0x3C	//Transfer image from MCU to display from the last written location
#define CMD_RD_MEM_AUTO			0x3E	//Read image data from the display continuing after the last CMD_RD_MEM_AUTO or CMD_RD_MEMSTART
#define CMD_SET_TEAR_SCANLINE	0x44	//Synchronisation information is sent from the display to the MCU when the device refresh reaches the provided scan line
#define CMD_GET_SCANLINE		0x45	//Get the current scan line
#define CMD_RD_DDB_START		0xA1	//Read the DDB from the provided location
#define CMD_RD_DDB_AUTO			0xA8	//
#define CMD_SET_PANEL_MODE		0xB0	//Set the LCD Panel Mode (RGB TFT or TTL)
#define CMD_GET_PANEL_MODE		0xB1	//Get the LCD Panel Mode, Pad strength & Resolution
#define CMD_SET_HOR_PERIOD		0xB4	//Set front porch
#define CMD_GET_HOR_PERIOD		0xB5	//Get current front porch setting
#define CMD_SET_VER_PERIOD		0xB6	//Set the vertical blanking interval between last scan line and next LFRAME pulse
#define CMD_GET_VER_PERIOD		0xB7	//Get the vertical blanking interval between last scan line and next LFRAME pulse
#define CMD_SET_GPIO_CONF		0xB8	//Set the GPIO configuration
#define CMD_GET_GPIO_CONF		0xB9	//Get the current GPIO configuration
#define CMD_SET_GPIO_VAL		0xBA	//Set GPIO value for GPIO configured as output
#define CMD_GET_GPIO_STATUS		0xBB	//Read current GPIO status
#define CMD_SET_POST_PROC		0xBC	//Set the image post processor
#define CMD_GET_POST_PROC		0xBD	//Get the image post processor
#define CMD_SET_PWM_CONF		0xBE	//Set the PWM configuration
#define CMD_GET_PWM_CONF		0xBF	//Get the PWM configuration
#define CMD_SET_LCD_GEN0		0xC0	//Set the Rise, Fall and Toggling properties of LCD Signal Generator 0
#define CMD_GET_LCD_GEN0		0xC1	//Get the current settings for LCD Signal Generator 0
#define CMD_SET_LCD_GEN1		0xC2	//Set the Rise, Fall and Toggling properties of LCD Signal Generator 1
#define CMD_GET_LCD_GEN1		0xC3	//Get the current settings for LCD Signal Generator 1
#define CMD_SET_LCD_GEN2		0xC4	//Set the Rise, Fall and Toggling properties of LCD Signal Generator 2
#define CMD_GET_LCD_GEN2		0xC5	//Get the current settings for LCD Signal Generator 2
#define CMD_SET_LCD_GEN3		0xC6	//Set the Rise, Fall and Toggling properties of LCD Signal Generator3
#define CMD_GET_LCD_GEN3		0xC7	//Get the current settings for LCD Signal Generator 3
#define CMD_SET_GPIO0_ROP		0xC8	//Set the GPIO0 with respect to the PCD signal generators using ROP3 operation
#define CMD_GET_GPIO0_ROP		0xC9	//Get the GPIO0 properties with respect to the LCD signal generators
#define CMD_SET_GPIO1_ROP		0xCA	//Set the GPIO1 with respect to the PCD signal generators using ROP3 operation
#define CMD_GET_GPIO1_ROP		0xCB	//Get the GPIO1 properties with respect to the LCD signal generators
#define CMD_SET_GPIO2_ROP		0xCC	//Set the GPIO2 with respect to the PCD signal generators using ROP3 operation
#define CMD_GET_GPIO2_ROP		0xCD	//Get the GPIO2 properties with respect to the LCD signal generators
#define CMD_SET_GPIO3_ROP		0xCE	//Set the GPIO3 with respect to the PCD signal generators using ROP3 operation
#define CMD_GET_GPIO3_ROP		0xCF	//Get the GPIO3 properties with respect to the LCD signal generators
#define CMD_SET_ABC_DBC_CONF	0xD0	//Set the Ambient back-light and dynamic back-light configuration
#define CMD_GET_ABC_DBC_CONF	0xD1	//Get the ambient back-light and current dynamic back-light configuration
#define CMD_SET_DBC_HISTO_PTR	0xD2	//
#define CMD_GET_DBC_HISTO_PTR	0xD3	//
#define CMD_SET_DBC_THRES		0xD4	//Set the threshold for each level of power saving
#define CMD_GET_DBC_THRES		0xD5	//Get the threshold for each level of power saving
#define CMD_SET_ABM_TMR			0xD6	//
#define CMD_GET_ABM_TMR			0xD7	//
#define CMD_SET_AMB_LVL0		0xD8	//
#define CMD_GET_AMB_LVL0		0xD9	//
#define CMD_SET_AMB_LVL1		0xDA	//
#define CMD_GET_AMB_LVL1		0xDB	//
#define CMD_SET_AMB_LVL2		0xDC	//
#define CMD_GET_AMB_LVL2		0xDD	//
#define CMD_SET_AMB_LVL3		0xDE	//
#define CMD_GET_AMB_LVL3		0xDF	//
#define CMD_PLL_START			0xE0	//Start the PLL
#define CMD_PLL_STOP			0xE1	//Disable the PLL
#define CMD_SET_PLL_MN			0xE2	//Set the PLL
#define CMD_GET_PLL_MN			0xE3	//Get the PLL settings
#define CMD_GET_PLL_STATUS		0xE4	//Get the current PLL status
#define CMD_ENT_DEEP_SLEEP		0xE5	//Set DEEP SLEEP mode
#define CMD_SET_PCLK			0xE6	//Set pixel clock (LSHIFT signal) frequency
#define CMD_GET_PCLK			0xE7	//Get pixel clock (LSHIFT signal) freq. settings
#define CMD_SET_DATA_INTERFACE	0xF0	//Set the Pixel data format of the parallel MCU interface
#define CMD_GET_DATA_INTERFACE	0xF1	//Get the current pixel data format settings

#endif /* SSD1963_CMD_H_ */
