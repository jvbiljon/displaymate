/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LCD_H
#define __LCD_H

#include "main.h"
#include "stm32f4xx_hal.h"
#include "Src/ESAL/norflash/M29W128.h"

#define LCD_BASE			((uint32_t)(0x60000000 | 0x08000000))
#define LCD					((LCDFSMC_TypeDef *) LCD_BASE)

#define LCD_Power_up_delay	500		//Delay from power up before

#define USE_16BIT_PMP
#define DISPLAY_CONTROLLER	SSD1963
#define COLOR_DEPTH			16
#define DISPLAY_PANEL		AM640480G2TNQW-W0H

#define LCD_DEFAULT_FONT    Font12x12

#ifndef DISPLAY_CONTROLLER
#error "Error: DISPLAY_CONTROLLER not defined"
#endif

//#define USE_57inch_TFT
#define USE_7inch_TFT

#if (DISPLAY_PANEL == AM640480G2TNQW-W0H)
	/*********************************************************************
	* Overview: Image orientation (can be 0, 90, 180, 270 degrees).
	*********************************************************************/
#define DISP_ORIENTATION    		0
	/*********************************************************************
	* Overview: Panel Data Width (R,G,B) in (5,6,5)
	*********************************************************************/
#define DISP_DATA_WIDTH             16
	/*********************************************************************
	* Overview: Horizontal and vertical display resolution
	*                  (from the glass Datasheet).
	*********************************************************************/
#ifdef USE_57inch_TFT
#define DISP_HOR_RESOLUTION 		640
#define DISP_VER_RESOLUTION 		480
#endif

#ifdef USE_7inch_TFT
#define DISP_HOR_RESOLUTION 		800
#define DISP_VER_RESOLUTION 		480
#endif
	/*********************************************************************
	* Overview: Horizontal synchronisation timing in pixels
	*                  (from the glass Datasheet).
	*********************************************************************/
#define DISP_HOR_PULSE_WIDTH		1
#define DISP_HOR_BACK_PORCH			210
#define DISP_HOR_FRONT_PORCH		45
	/*********************************************************************
	* Overview: Vertical synchronisation timing in lines
	*                  (from the glass Datasheet).
	*********************************************************************/
#define DISP_VER_PULSE_WIDTH		1
#define DISP_VER_BACK_PORCH			34
#define DISP_VER_FRONT_PORCH		10
	/* End of definition for DISPLAY_PANEL == AM640480G2TNQW-W0H */

#else
#error "Graphics controller is not defined"
#endif // DISPLAY_CONTROLLER == ABC


/**
 * @LCD Messages and Parameters
  */
#define Terminal_Border  	0
#define Text_to_Border 		8
#define Font1_spacing 		4
#define Font2_spacing 		4

/* Line Space Definitions for 16x24 font */
#define LINE1				0
#define LINE2				24
#define LINE3				48
#define LINE4				72
#define LINE5				96
#define LINE6				120
#define LINE7				144
#define LINE8				168
#define LINE9				192
#define LINE10				216
#define LINE11				240
#define LINE12				264
#define LINE13				288
#define LINE14				312
#define LINE15				336
#define LINE16				360
#define LINE17				384
#define LINE18				408
#define LINE19				432
#define LINE20				456

#define RGB565CONVERT(red, green, blue) (uint16_t) (((red >> 3) << 11) | ((green >> 2) << 5) | (blue >> 3))

/*********************************************************************
* Overview: Some basic colour definitions.
*********************************************************************/
#define BLACK               RGB565CONVERT(0,    0,      0)
#define BRIGHTBLUE          RGB565CONVERT(0,    0,      255)
#define BRIGHTGREEN         RGB565CONVERT(0,    255,    0)
#define BRIGHTCYAN          RGB565CONVERT(0,    255,    255)
#define BRIGHTRED           RGB565CONVERT(255,  0,      0)
#define BRIGHTMAGENTA       RGB565CONVERT(255,  0,      255)
#define BRIGHTYELLOW        RGB565CONVERT(255,  255,    0)
#define BLUE                RGB565CONVERT(0,    0,      128)
#define GREEN               RGB565CONVERT(0,    128,    0)
#define CYAN                RGB565CONVERT(0,    128,    128)
#define RED                 RGB565CONVERT(192,  0,      0)
#define MAGENTA             RGB565CONVERT(128,  0,      128)
#define BROWN               RGB565CONVERT(139,  69,     19)
#define LIGHTGRAY           RGB565CONVERT(128,  128,    128)
#define DARKGRAY            RGB565CONVERT(64,   64,     64)
#define LIGHTBLUE           RGB565CONVERT(128,  128,    255)
#define LIGHTGREEN          RGB565CONVERT(128,  255,    128)
#define LIGHTCYAN           RGB565CONVERT(128,  255,    255)
#define LIGHTRED            RGB565CONVERT(255,  128,    128)
#define LIGHTMAGENTA        RGB565CONVERT(255,  128,    255)
#define AMBER				RGB565CONVERT(255,  204,    153)
#define YELLOW              RGB565CONVERT(255,  255,    128)
#define WHITE               RGB565CONVERT(255,  255,    255)

#define GRAY0       	    RGB565CONVERT(224,  224,    224)
#define GRAY1         	    RGB565CONVERT(192,  192,    192)
#define GRAY2               RGB565CONVERT(160,  160,    160)
#define GRAY3               RGB565CONVERT(128,  128,    128)
#define GRAY4               RGB565CONVERT(96,   96,     96)
#define GRAY5               RGB565CONVERT(64,   64,     64)
#define GRAY6	            RGB565CONVERT(32,   32,     32)

enum
{
	SMALL_FONT = 0,
	MEDIUM_FONT,
	LARGE_FONT
};

typedef enum
{
	imJetChargeLogo = 0,
	imHomerSimpson
} IMAGES_tenBitMaps;

typedef struct
{
	__IO uint16_t LCD_COMMAND;
	__IO uint16_t LCD_DATA;
} LCDFSMC_TypeDef;

void LCD_vInit(void);
void LCD_vWriteCommand(uint8_t);
void LCD_vWriteData(uint16_t);
void LCD_vSetBacklight(uint8_t);
void LCD_vClearScreen(uint32_t);
void LCD_vSetDisplayArea(uint16_t, uint16_t, uint16_t, uint16_t);
void LCD_vPutChar(uint16_t, uint16_t, uint8_t, const uint16_t *, uint16_t, uint16_t, int);
void LCD_vPrintText(uint16_t, uint16_t, char *, uint16_t, uint16_t, uint16_t, int);
void LCD_vDrawRectangleFill(uint16_t, uint16_t, uint16_t, uint16_t, uint32_t);
void LCD_vDrawBitmap(uint16_t, uint16_t, IMAGES_tenBitMaps);

void LCD_vDisplayHomescreen(void);

#endif /* __INIT_H */
