#include "lcd.h"
#include "lcd_commands.h"
#include "fonts.h"

uint16_t SmallFontCharSize = 24;
uint16_t SmallFontWidth = 12;
uint16_t SmallFontHeight = 12;

uint16_t MediumFontCharSize = 48;
uint16_t MediumFontWidth = 16;
uint16_t MediumFontHeight = 24;

void LCD_vInit(void)
{
	LCD_vWriteCommand(CMD_SET_PLL_MN);
#ifdef USE_57inch_TFT
	LCD_vWriteData(0x31); 						// PLLclk = REFclk * 50 (500MHz)
	LCD_vWriteData(0x04); 						// SYSclk = PLLclk / 5 (100MHz)
	LCD_vWriteData(0x54); 						// dummy
#endif

#ifdef USE_7inch_TFT
	LCD_vWriteData(0x23); 						// PLLclk = REFclk * 100 (1000MHz)
	LCD_vWriteData(0x02); 						// SYSclk = PLLclk / 10 (100MHz)
	LCD_vWriteData(0x54); 						// dummy
#endif

	LCD_vWriteCommand(CMD_PLL_START);
	LCD_vWriteData(1); 							// Use PLL output as system clock

	HAL_Delay(1); 								// Delay 1ms
	LCD_vWriteCommand(CMD_PLL_START);
	LCD_vWriteData(3); 							// Enable PLL
	HAL_Delay(5);  								// Delay 5ms

	LCD_vWriteCommand(CMD_SOFT_RESET);
	HAL_Delay(5);  								// Delay 5ms
	LCD_vWriteCommand(CMD_EXIT_SLEEP);
	HAL_Delay(120);  							// Delay 120ms

	LCD_vWriteCommand(CMD_SET_PCLK); 			// Set LEFT SHIFT Frequency
	LCD_vWriteData(0x04);
	LCD_vWriteData(0xFF);
	LCD_vWriteData(0xFF);

	HAL_Delay(5);  								// Delay 5ms
	LCD_vWriteCommand(CMD_SET_PANEL_MODE);
	LCD_vWriteData(0x10);
	LCD_vWriteData(0x80);
	LCD_vWriteData((DISP_HOR_RESOLUTION - 1) >> 8);
	LCD_vWriteData(DISP_HOR_RESOLUTION - 1);
	LCD_vWriteData((DISP_VER_RESOLUTION - 1) >> 8);
	LCD_vWriteData(DISP_VER_RESOLUTION - 1);
	LCD_vWriteData(0x00);

	LCD_vWriteCommand(CMD_SET_HOR_PERIOD);
#define HT (DISP_HOR_RESOLUTION + DISP_HOR_PULSE_WIDTH + DISP_HOR_BACK_PORCH + DISP_HOR_FRONT_PORCH)
	LCD_vWriteData((HT - 1) >> 8);
	LCD_vWriteData(HT - 1);
#define HPS (DISP_HOR_PULSE_WIDTH + DISP_HOR_BACK_PORCH)
	LCD_vWriteData((HPS - 1) >> 8);
	LCD_vWriteData(HPS - 1);
	LCD_vWriteData(DISP_HOR_PULSE_WIDTH - 1);
	LCD_vWriteData(0x00);
	LCD_vWriteData(0x00);
	LCD_vWriteData(0x00);

	LCD_vWriteCommand(CMD_SET_VER_PERIOD);
#define VT (DISP_VER_PULSE_WIDTH + DISP_VER_BACK_PORCH + DISP_VER_FRONT_PORCH + DISP_VER_RESOLUTION)
	LCD_vWriteData((VT - 1) >> 8);
	LCD_vWriteData(VT - 1);
#define VSP (DISP_VER_PULSE_WIDTH + DISP_VER_BACK_PORCH)
	LCD_vWriteData((VSP - 1) >> 8);
	LCD_vWriteData(VSP - 1);
	LCD_vWriteData(DISP_VER_PULSE_WIDTH - 1);
	LCD_vWriteData(0x00);
	LCD_vWriteData(0x00);

	LCD_vWriteCommand(CMD_SET_ADDR_MODE);
	LCD_vWriteData(0x80);

	//Set pixel format, i.e. the bpp
	LCD_vWriteCommand(0x3A);
	LCD_vWriteData(0x55);  						// set 16bpp

	LCD_vWriteCommand(CMD_SET_DATA_INTERFACE);
	LCD_vWriteData(3); 							// 16-bit (565 Format)

	LCD_vWriteCommand(CMD_ON_DISPLAY);

	LCD_vSetBacklight(0xFF);  

}

void LCD_vWriteCommand(uint8_t cmd)
{
	LCD->LCD_COMMAND = cmd;
}

void LCD_vWriteData(uint16_t data)
{
	LCD->LCD_DATA = data;
}

void LCD_vSetBacklight(uint8_t u8Intensity)
{
	LCD_vWriteCommand(CMD_SET_PWM_CONF);		// Set PWM configuration for backlight control
	LCD_vWriteData(0x0E); 					// PWMF[7:0] = 2, PWM base freq = PLL/(256*(1+5))/256 =
											// 300Hz for a PLL freq = 120MHz
	LCD_vWriteData(u8Intensity); 			// Set duty cycle, from 0x00 (total pull-down) to 0xFF
											// (99% pull-up , 255/256)
	LCD_vWriteData(0x01); 					// PWM enabled and controlled by host (mcu)
	LCD_vWriteData(0x00);
	LCD_vWriteData(0x00);
	LCD_vWriteData(0x00);
}

void LCD_vSetDisplayArea(uint16_t sx, uint16_t ex, uint16_t sy, uint16_t ey)
{
	LCD_vWriteCommand(CMD_SET_COLUMN);
	LCD_vWriteData((sx & 0xFF00) >> 8);
	LCD_vWriteData(sx & 0x00FF);
	LCD_vWriteData((ex & 0xFF00) >> 8);
	LCD_vWriteData(ex & 0x00FF);

	LCD_vWriteCommand(CMD_SET_PAGE);
	LCD_vWriteData((sy & 0xFF00) >> 8);
	LCD_vWriteData(sy & 0x00FF);
	LCD_vWriteData((ey & 0xFF00) >> 8);
	LCD_vWriteData(ey & 0x00FF);
}

void LCD_vClearScreen(uint32_t colour)
{
	uint16_t u16x, u16y;

	LCD_vSetDisplayArea(0, DISP_HOR_RESOLUTION - 1, 0, DISP_VER_RESOLUTION - 1);
	LCD_vWriteCommand(CMD_WR_MEMSTART);
	for (u16x = 0; u16x < DISP_HOR_RESOLUTION; u16x++)
	{
		for (u16y = 0; u16y < DISP_VER_RESOLUTION; u16y++)
		{
			LCD_vWriteData(colour);
		}
	}
}

/**
 * @brief  Write character on screen
 *  x -> Start point on X-Axis for Text
 *  y -> Start point on Y-Axis for Text
 *  Character -> ASCii character to dislpay
 *  TextColor -> Colour to use for Text
 *  BackColor -> Colour to use for Text Background
 *  Font -> SMALL_FONT = Ascii_12x12, MEDIUM_FONT = Ascii_16x24
 * @retval None
 */
void LCD_vPutChar(uint16_t x, uint16_t y, uint8_t Character, const uint16_t *c, uint16_t TextColor, uint16_t BackColor, int Font)
{
	int row = 0;
	int column = 0;

	switch (Font)
	{
	case SMALL_FONT:
		{
			LCD_vWriteCommand(CMD_SET_ADDR_MODE);
			LCD_vWriteData(0b00000000);
			LCD_vSetDisplayArea(x, x + SmallFontWidth - 1, y, y + SmallFontHeight - 1);
			LCD_vWriteCommand(CMD_WR_MEMSTART);

			for (row = 0; row < SmallFontHeight; row++)
			{
				for (column = 0; column < SmallFontWidth; column++)
				{
					if ((((c[row - 32 * 12] & ((0x80 << ((SmallFontWidth / 12) * 8)) >> column)) == 0x00) && (SmallFontWidth <= 12)) ||
							(((c[row - 32 * 12] & (0x1 << column)) == 0x00) && (SmallFontWidth > 12)))

					{
						LCD_vWriteData(BackColor);
					}
					else
					{
						LCD_vWriteData(TextColor);
					}
				}
			}
			LCD_vWriteCommand(CMD_SET_ADDR_MODE);
			LCD_vWriteData(0x80);
		}
		break;
	case MEDIUM_FONT:
		{
			LCD_vWriteCommand(CMD_SET_ADDR_MODE);
			LCD_vWriteData(0b01000000);
			LCD_vSetDisplayArea(x, x + MediumFontWidth - 1, y, y + MediumFontHeight - 1);
			LCD_vWriteCommand(CMD_WR_MEMSTART);

			for (row = 0; row < MediumFontHeight; row++)
			{
				for (column = 0; column < MediumFontWidth; column++)
				{
					if ((((c[row - 32 * 24] & ((0x80 << ((MediumFontWidth / 16) * 8)) >> column)) == 0x00) &&(MediumFontWidth <= 16)) ||
							(((c[row - 32 * 24] & (0x1 << column)) == 0x00) && (MediumFontWidth > 16)))

					{
						LCD_vWriteData(BackColor);
					}
					else
					{
						LCD_vWriteData(TextColor);
					}
				}
			}
			LCD_vWriteCommand(CMD_SET_ADDR_MODE);
			LCD_vWriteData(0x80);
		}
		break;
	default:
		break;
	}
}

/**
 * @brief  Write String of characters to screen
 *  x -> Start point on X-Axis for Text
 *  y -> Start point on Y-Axis for Text
 *  *str -> Pointer to string
 *  len -> Length of string to be written
 *  TextColor -> Colour to use for Text
 *  BackColor -> Colour to use for Text Background
 *  Font -> SMALL_FONT = Ascii_12x12, MEDIUM_FONT = Ascii_16x24
 * @retval None
 */
void LCD_vPrintText(uint16_t x, uint16_t y, char *str, uint16_t len, uint16_t TextColor, uint16_t BackColor, int Font)
{
	uint16_t u16i;

	switch (Font)
	{
	case SMALL_FONT:
		for (u16i = 0; u16i < len; u16i++)
		{
			LCD_vPutChar((x + SmallFontWidth * u16i), y, *str, &ASCII12x12_Table[*str * SmallFontHeight], TextColor, BackColor, Font);
			str++;
		}
		break;
	case MEDIUM_FONT:
		for (u16i = 0; u16i < len; u16i++)
		{
			LCD_vPutChar((x + MediumFontWidth * u16i), y, *str, &ASCII16x24_Table[(*str) * MediumFontHeight], TextColor, BackColor, Font);
			str++;
		}
		break;
	default:
		break;
	}
}

/**
 * @brief  Draw FILLED Rectangle on screen
 *  x0 -> Start point on X-Axis of Rectangle
 *  x1 -> End point on X-Axis of Rectangle
 *  y0 -> Start point on Y-Axis of Rectangle
 *  y1 -> End point on Y-Axis of Rectangle
 *  Fill Colour
 * @retval None
 */
void LCD_vDrawRectangleFill(uint16_t x0, uint16_t x1, uint16_t y0, uint16_t y1, uint32_t colour)
{
	uint16_t x, y;

	LCD_vSetDisplayArea(x0, x1, y0, y1);
	LCD_vWriteCommand(CMD_WR_MEMSTART);
	for (y = y0; y <= y1; y++)
	{
		for (x = x0; x <= x1; x++)
		{
			LCD_vWriteData(colour);
		}
	}
	LCD_vSetDisplayArea(0, DISP_HOR_RESOLUTION - 1, 0, DISP_VER_RESOLUTION - 1);
}

void LCD_vDrawBitmap(uint16_t x, uint16_t y, IMAGES_tenBitMaps image)
{
	uint16_t u16i = 0;
	uint16_t ImageBuf1[1024 / 2] = { 0 };
	uint32_t ImageAddress = NOR_BLOCK_0;
	
	switch (image)
	{
	case imJetChargeLogo:
		{
			ImageAddress = NOR_BLOCK_0;
			LCD_vSetDisplayArea(x, x + 703, y, y + 159);
			LCD_vWriteCommand(CMD_WR_MEMSTART);
			while (ImageAddress < NOR_BLOCK_2)
			{
				NOR_u8ReadBuffer(ImageBuf1, ImageAddress, 512);
				for (u16i = 0; u16i < 512; u16i++)
				{
					LCD_vWriteData(ImageBuf1[u16i]);
				}
				ImageAddress += (1024);  
			}
		}
		break;
	case imHomerSimpson:
		{
			ImageAddress = NOR_BLOCK_119;
			LCD_vSetDisplayArea(x, x + 279, y, y + 208);
			LCD_vWriteCommand(CMD_WR_MEMSTART);
			while (ImageAddress < NOR_BLOCK_120)
			{
				NOR_u8ReadBuffer(ImageBuf1, ImageAddress, 512);
				for (u16i = 0; u16i < 512; u16i++)
				{
					LCD_vWriteData(ImageBuf1[u16i]);
				}
				ImageAddress += (1024);  
			}
		}
		break;
	default:
		break;
	}
}

void LCD_vDisplayHomescreen(void)
{
	LCD_vClearScreen(WHITE);
	LCD_vPrintText(0, LINE1, "Jetcharge Chargemate", sizeof("Jetcharge Chargemate") - 1, BLACK, WHITE, MEDIUM_FONT);
	
	LCD_vDrawBitmap(50, 160, imJetChargeLogo);
}