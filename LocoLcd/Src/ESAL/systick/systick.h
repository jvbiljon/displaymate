/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SYSTICK_H
#define __SYSTICK_H

#include "main.h"
#include "stm32f4xx_hal.h"

void SYSTCK_vHandleSystickFunctions(void);

/* LED */
uint16_t SYSTCK_u16GetDelayCounter(void);
void SYSTCK_vClearDelayCounter(void);
/* GPIO */
uint16_t SYSTCK_u16GetGPIOCounter(void);
void SYSTCK_vClearGPIOCounter(void);
/* DELAY */
uint16_t SYSTCK_u16GetDelayCounterPortA(void);
void SYSTCK_vClearDelayCounterPortA(void);
uint16_t SYSTCK_u16GetDelayCounterPortB(void);
void SYSTCK_vClearDelayCounterPortB(void);
/* DIGITAL INPUT DEBOUNCE */
uint16_t SYSTCK_u16GetDebouceCounter(void);
void SYSTCK_vClearDebounceCounter(void);
/* ADC DEBOUNCE */
uint16_t SYSTCK_u16GetAdcReadCounterPortA(void);
void SYSTCK_vClearAdcReadCounterPortA(void);
uint16_t SYSTCK_u16GetAdcReadCounterPortB(void);
void SYSTCK_vClearAdcReadCounterPortB(void);
/* MODBUS */
uint16_t SYSTCK_u16GetMODBUSWaitCounter(void);
void SYSTCK_vClearMODBUSWaitCounter(void);
uint16_t SYSTCK_u16GetMODBUSRequestCounter(void);
void SYSTCK_vClearMODBUSRequestCounter(void);
/* WIFI */
uint16_t SYSTCK_u16GetWifiWaitCounter(void);
void SYSTCK_vClearWifiWaitCounter(void);
uint16_t SYSTCK_u16GetWifiCommandResponseTimeout(void);
void SYSTCK_vClearWifCommandResponseTimeout(void);
/* WEBSOCKET */
uint16_t SYSTCK_u16GetWebsocketCommandResponseTimeout(void);
void SYSTCK_vClearWebsocketommandResponseTimeout(void);
uint16_t SYSTCK_u16GetWebsocketPingCounter(void);
void SYSTCK_vClearWifWebsocketPingTimeout(void);
uint16_t SYSTCK_u16GetWebsocketTimeRequestCounter(void);
void SYSTCK_vClearWebsocketTimeRequestCounter(void);

#endif /* __SYSTICK_H */
