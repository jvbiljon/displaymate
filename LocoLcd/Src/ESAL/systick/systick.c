#include "Src/ESAL/systick/systick.h"

volatile uint16_t u16DelayCounter = 0;

/* Increment Counter every 1ms */
void SYSTCK_vHandleSystickFunctions(void)
{
	u16DelayCounter++;
}

uint16_t SYSTCK_u16GetDelayCounter(void)
{
	return u16DelayCounter;
}

void SYSTCK_vClearDelayCounter(void)
{
	u16DelayCounter = 0;
}

