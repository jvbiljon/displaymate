/**
 ******************************************************************************
 * @file    usbh_usr.c
 * @author  MCD Application Team
 * @version V2.0.0
 * @date    22-July-2011
 * @brief   This file includes the usb host library user callbacks
 ******************************************************************************
 * @attention
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
 ******************************************************************************
 */

#define IMAGE_BUFFER_SIZE    512

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "usbh_usr.h"
#include "ff.h"       /* FATFS */
#include "usbh_msc_core.h"
#include "usbh_msc_scsi.h"
#include "usbh_msc_bot.h"

/* Private Macro's -----------------------------------------------------------*/
extern USB_OTG_CORE_HANDLE          USB_OTG_Core;

/* Private Variables ---------------------------------------------------------*/
uint8_t USBH_USR_ApplicationState = USH_USR_FS_INIT;
uint8_t filenameString[15] = {0};
uint8_t Image_Buf[IMAGE_BUFFER_SIZE];
//uint16_t NORBuffer[IMAGE_BUFFER_SIZE / 2];
//uint16_t NORImBuffer[IMAGE_BUFFER_SIZE];

FATFS fatfs;
FIL file;
uint8_t line_idx = 0;   
uint8_t ImageCount = 0;
uint8_t F_WaitReboot = 0;

extern short int F_WriteFile;
extern short int F_ReadList;
extern short int F_ReadFile;

//extern uint8_t writeTextBuff[];// = "  STM32 USB OTG File created on USB Drive by... \r\n\r\nPixonix Engineering!";

/*  Points to the DEVICE_PROP structure of current device */
/*  The purpose of this register is to speed up the execution */
USBH_Usr_cb_TypeDef USR_cb =
{
		USBH_USR_Init,
		USBH_USR_DeInit,
		USBH_USR_DeviceAttached,
		USBH_USR_ResetDevice,
		USBH_USR_DeviceDisconnected,
		USBH_USR_OverCurrentDetected,
		USBH_USR_DeviceSpeedDetected,
		USBH_USR_Device_DescAvailable,
		USBH_USR_DeviceAddressAssigned,
		USBH_USR_Configuration_DescAvailable,
		USBH_USR_Manufacturer_String,
		USBH_USR_Product_String,
		USBH_USR_SerialNum_String,
		USBH_USR_EnumerationDone,
		USBH_USR_UserInput,
		USBH_USR_MSC_Application,
		USBH_USR_DeviceNotSupported,
		USBH_USR_UnrecoveredError
};

/* Private Constants --------------------------------------------------------*/
/* Private Function Prototypes ----------------------------------------------*/

/**
 * @brief  USBH_USR_Init
 *         Displays the message on LCD for host lib initialization
 * @param  None
 * @retval None
 */
void USBH_USR_Init(void)
{
	static uint8_t startup = 0;
	#define String "USB On-the-Go Initialised\r\n"

	if(startup == 0)
	{
		startup = 1;
		USART_SendString(PC, String);
//		LCD_Text(410, 30, String, (sizeof(String) - 1), CYAN, BLACK, 1);
	}
}

/**
 * @brief  USBH_USR_DeviceAttached
 *         Displays the message on LCD on device attached
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceAttached(void)
{
	#define String1 "USB Device Attached\r\n"

	USART_SendString(PC, String1);
//	LCD_Text(410, 70, String1, (sizeof(String1) - 3), GREEN, BLACK, 1);
}

/**
 * @brief  USBH_USR_UnrecoveredError
 * @param  None
 * @retval None
 */
void USBH_USR_UnrecoveredError(void)
{
	#define String2 "ERROR - UN-recovered Error\r\n"

	USART_SendString(PC, String2);
//	LCD_Text(410, 70, String2, (sizeof(String2) - 3), BRIGHTRED, BLACK, 1);
}

/**
 * @brief  USBH_DisconnectEvent
 *         Device disconnect event
 * @param  None
 * @retval Staus
 */
void USBH_USR_DeviceDisconnected(void)
{
	#define String3 "USB Device Disconnected\r\n"
	#define String4 "                       \r\n"

	if (F_WaitReboot == 1)
	{
		USART_SendString(PC, "Re-booting System...\r\n");
		Delay(1000);
		NVIC_SystemReset();
	}
	else
	{
		USART_SendString(PC, String3);
	}
//	LCD_Text(410, 50, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 70, String3, (sizeof(String3) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 90, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 110, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 130, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 150, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 170, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 190, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 210, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 230, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 250, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 270, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 290, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 310, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 330, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
//	LCD_Text(410, 350, String4, (sizeof(String4) - 3), BRIGHTRED, BLACK, 1);
}

/**
 * @brief  USBH_USR_ResetUSBDevice
 * @param  None
 * @retval None
 */
void USBH_USR_ResetDevice(void)
{
	#define String5 "USB Device Reset\r\n"

	USART_SendString(PC, String5);
//	LCD_Text(410, 50, String5, (sizeof(String5) - 1), MAGENTA, BLACK, 1);
}

/**
 * @brief  USBH_USR_DeviceSpeedDetected
 *         Displays the message on LCD for device speed
 * @param  Device speed
 * @retval None
 */
void USBH_USR_DeviceSpeedDetected(uint8_t DeviceSpeed)
{
	#define String6 "USB High Speed Device\r\n"
	#define String7 "USB Full Speed Device\r\n"
	#define String8 "USB Low Speed Device\r\n"
	#define String9 "ERROR - Speed Detect Error\r\n"

	if(DeviceSpeed == HPRT0_PRTSPD_HIGH_SPEED)
	{
		USART_SendString(PC, String6);
//		LCD_Text(410, 90, String6, (sizeof(String6) - 3), LIGHTBLUE, BLACK, 1);
	}
	else if(DeviceSpeed == HPRT0_PRTSPD_FULL_SPEED)
	{
		USART_SendString(PC, String7);
//		LCD_Text(410, 90, String7, (sizeof(String7) - 3), LIGHTBLUE, BLACK, 1);
	}
	else if(DeviceSpeed == HPRT0_PRTSPD_LOW_SPEED)
	{
		USART_SendString(PC, String8);
//		LCD_Text(410, 90, String8, (sizeof(String8) - 3), LIGHTBLUE, BLACK, 1);
	}
	else
	{
		USART_SendString(PC, String9);
//		LCD_Text(410, 90, String9, (sizeof(String9) - 3), BRIGHTRED, BLACK, 1);
	}
}

/**
 * @brief  USBH_USR_Device_DescAvailable
 *         Displays the message on LCD for device descriptor
 * @param  device descriptor
 * @retval None
 */
void USBH_USR_Device_DescAvailable(void *DeviceDesc)
{ 
	USBH_DevDesc_TypeDef *hs;
	hs = DeviceDesc;

	//  LCD_UsrLog("VID : %04Xh\n" , (uint32_t)(*hs).idVendor);
	//  LCD_UsrLog("PID : %04Xh\n" , (uint32_t)(*hs).idProduct);
}

/**
 * @brief  USBH_USR_DeviceAddressAssigned
 *         USB device is successfully assigned the Address
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceAddressAssigned(void)
{

}


/**
 * @brief  USBH_USR_Conf_Desc
 *         Displays the message on LCD for configuration descriptor
 * @param  Configuration descriptor
 * @retval None
 */
void USBH_USR_Configuration_DescAvailable(USBH_CfgDesc_TypeDef * cfgDesc,
		USBH_InterfaceDesc_TypeDef *itfDesc,
		USBH_EpDesc_TypeDef *epDesc)
{
	USBH_InterfaceDesc_TypeDef *id;

	id = itfDesc;

	if((*id).bInterfaceClass  == 0x08)
	{
		//    LCD_UsrLog((void *)MSG_MSC_CLASS);
	}
	else if((*id).bInterfaceClass  == 0x03)
	{
		//    LCD_UsrLog((void *)MSG_HID_CLASS);
	}
}

/**
 * @brief  USBH_USR_Manufacturer_String
 *         Displays the message on LCD for Manufacturer String
 * @param  Manufacturer String
 * @retval None
 */
void USBH_USR_Manufacturer_String(void *ManufacturerString)
{
	//  LCD_UsrLog("Manufacturer : %s\n", (char *)ManufacturerString);
}

/**
 * @brief  USBH_USR_Product_String
 *         Displays the message on LCD for Product String
 * @param  Product String
 * @retval None
 */
void USBH_USR_Product_String(void *ProductString)
{
	//  LCD_UsrLog("Product : %s\n", (char *)ProductString);
}

/**
 * @brief  USBH_USR_SerialNum_String
 *         Displays the message on LCD for SerialNum_String
 * @param  SerialNum_String
 * @retval None
 */
void USBH_USR_SerialNum_String(void *SerialNumString)
{
	//  LCD_UsrLog( "Serial Number : %s\n", (char *)SerialNumString);
} 

/**
 * @brief  EnumerationDone
 *         User response request is displayed to ask application jump to class
 * @param  None
 * @retval None
 */
void USBH_USR_EnumerationDone(void)
{
	#define  String10 "Enumeration Complete\r\n"

	USART_SendString(PC, String10);
//	LCD_Text(410, 110, String10, (sizeof(String10) - 3), BRIGHTYELLOW, BLACK, 1);
} 


/**
 * @brief  USBH_USR_DeviceNotSupported
 *         Device is not supported
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceNotSupported(void)
{
	#define String11 "ERROR - Dev. NOT Supported\r\n"

	USART_SendString(PC, String11);
//	LCD_Text(410, 110, String11, (sizeof(String11) - 3), BRIGHTRED, BLACK, 1);
}  


/**
 * @brief  USBH_USR_UserInput
 *         User Action for application state entry
 * @param  None
 * @retval USBH_USR_Status : User response for key button
 */
USBH_USR_Status USBH_USR_UserInput(void) //jvbiljon
{
	USBH_USR_Status usbh_usr_status;

	usbh_usr_status = USBH_USR_RESP_OK;//USBH_USR_NO_RESP;

	return usbh_usr_status;
}  

/**
 * @brief  USBH_USR_OverCurrentDetected
 *         Over Current Detected on VBUS
 * @param  None
 * @retval Staus
 */
void USBH_USR_OverCurrentDetected (void)
{
	#define String12 "OVERCURRENT Detected!!!\r\n"

	USART_SendString(PC, String12);
//	LCD_Text(410, 110, String12, (sizeof(String12) - 3), BRIGHTRED, BLACK, 1);
}


/**
 * @brief  USBH_USR_MSC_Application
 *         Demo application for mass storage
 * @param  None
 * @retval Staus
 */
int USBH_USR_MSC_Application(void)
{
	#define  String13 "ERROR - Cannot Init FAT FS\r\n"
	#define  String14 "FAT FS Initialisation Done\r\n"
	#define  String15 "ERROR - Disk Write Protect\r\n"
	#define  String16 "Writing File...\r\n"

//	LCD_Text(410, 130, String1, (sizeof(String1) - 1), LIGHTMAGENTA, BLACK);

	switch(USBH_USR_ApplicationState)
	{
	case USH_USR_FS_INIT:
		/* Initializes the File System*/
		if (f_mount( 0, &fatfs ) != FR_OK)
		{
			USART_SendString(PC, String13);
//			LCD_Text(410, 130, String13, (sizeof(String13) - 3), BRIGHTRED, BLACK, 1);
			return(-1);
		}
		USART_SendString(PC, String14);
//		LCD_Text(410, 130, String14, (sizeof(String14) - 3), LIGHTGREEN, BLACK, 1);

		if(USBH_MSC_Param.MSWriteProtect == DISK_WRITE_PROTECTED)
		{
			USART_SendString(PC, String15);
//			LCD_Text(410, 150, String15, (sizeof(String15) - 3), BRIGHTRED, BLACK, 1);
		}

		USBH_USR_ApplicationState = USH_USR_FS_WAITCOMMAND;

		//set flag to read files
		F_ReadList = 1;
		break;
	case USH_USR_FS_READLIST:
		line_idx = 0;
		Explore_Disk_Images("0:/", 1);
		USBH_USR_ApplicationState = USH_USR_FS_WAITCOMMAND;
		break;
	case USH_USR_FS_WRITEFILE:
		USART_SendString(PC, String16);
//		LCD_Text(410, 150, String16, (sizeof(String16) - 3), LIGHTGRAY, BLACK, 1);
		USB_OTG_BSP_mDelay(100);

		if(USBH_MSC_Param.MSWriteProtect == DISK_WRITE_PROTECTED)
		{
			USART_SendString(PC, String15);
//			LCD_Text(410, 130, String15, (sizeof(String15) - 3), BRIGHTRED, BLACK, 1);
			USBH_USR_ApplicationState = USH_USR_FS_WAITCOMMAND;
			break;
		}

		WriteFile();
		USBH_USR_ApplicationState = USH_USR_FS_INIT;
		break;
	case USH_USR_FS_DRAW:
		while(HCD_IsDeviceConnected(&USB_OTG_Core))
		{
			if (f_mount(0, &fatfs ) != FR_OK )
			{

				return(-1);
			}
			return 1;//Image_Browser("0:/");
		}
		break;
	case USH_USR_FS_READFILE:
		while(HCD_IsDeviceConnected(&USB_OTG_Core))
		{
			if (f_mount(0, &fatfs ) != FR_OK )
			{
				/* fat_fs initialisation fails */
				F_ReadFile = 0;
				return(-1);
			}
			F_ReadFile = 0;
			EraseImageFlash();
			return Image_Browser("0:/");
		}
		F_ReadFile = 0;
		break;
	case USH_USR_FS_WAITCOMMAND:
		if (F_WriteFile == 1)
		{
			USBH_USR_ApplicationState = USH_USR_FS_WRITEFILE;
		}

		if (F_ReadList == 1)
		{
			USBH_USR_ApplicationState = USH_USR_FS_READLIST;
		}

		if (F_ReadFile == 1)
		{
			USBH_USR_ApplicationState = USH_USR_FS_READFILE;
		}
		break;
	default:
		break;
	}
	return(0);
}

/**
 * @brief  USBH_USR_DeInit
 *         Deint User state and associated variables
 * @param  None
 * @retval None
 */
void USBH_USR_DeInit(void)
{
	USBH_USR_ApplicationState = USH_USR_FS_INIT;
}

/**
 * @brief  WriteFile
 *         Write a file on the USB Flash Drive
 * @param  None
 * @retval None
 */
void WriteFile(void)
{
	FRESULT res;
	uint8_t writeTextBuff[] = "\r\nSTM32 USB OTG File created on USB Drive by... \r\n\r\nPixonix Engineering!";
	uint16_t bytesWritten, bytesToWrite;
	#define String17 "Writing File...  ERROR!\r\n"
	#define String18 "Writing File...  Done!\r\n"

	USB_OTG_BSP_mDelay(100);
	/* Register work area for logical drives */
	f_mount(0, &fatfs);

	if(f_open(&file, "0:/ME_ALSMS_Logs/Pixonix.TXT", FA_CREATE_ALWAYS | FA_WRITE) == FR_OK)
	{
		/* Write buffer to file */
		bytesToWrite = sizeof(writeTextBuff);
		res = f_write(&file, writeTextBuff, bytesToWrite, (void *)&bytesWritten);

		if((bytesWritten == 0) || (res != FR_OK)) /*EOF or Error*/
		{
			USART_SendString(PC, String17);
//			LCD_Text(410, 150, String17, (sizeof(String17) - 3), BRIGHTRED, BLACK, 1);
		}
		else
		{
			USART_SendString(PC, String18);
//			LCD_Text(410, 150, String18, (sizeof(String18) - 1), LIGHTGRAY, BLACK, 1);
		}

		/*close file and filesystem*/
		f_close(&file);
		f_mount(0, NULL);
	}
	else
	{
		/* File created in DISK */
	}
	// Clear Flag
	F_WriteFile = 0;
}

/**
* @brief  Explore_Disk
*         Displays disk content
* @param  path: pointer to root path
* @retval None
*/
uint8_t Explore_Disk_Images(char* path, uint8_t recu_level)
{
	#define String19 "Exploring Images in ROOT...   \r\n"
	#define String20 "Images does not Exist         \r\n"
	#define String21 "                              \r\n"

	FRESULT res;
	FILINFO fno;
	DIR dir;
	char *fn;
#if _USE_LFN
	static char lfn[_MAX_LFN * (_DF1S ? 2 : 1) + 1];
	fno.lfname = lfn;
	fno.lfsize = sizeof(lfn);
#endif

	int i = 0;
	int j = 0;

	uint8_t String50[] = "                              ";

	USART_SendString(PC, String19);
//	LCD_Text(410, 190, String1, (sizeof(String19) - 3), LIGHTGRAY, BLACK, 1);
//	LCD_Text(410, 210, String4, (sizeof(String21) - 3), LIGHTGRAY, BLACK, 1);
//	LCD_Text(410, 230, String4, (sizeof(String21) - 3), LIGHTGRAY, BLACK, 1);
//	LCD_Text(410, 250, String4, (sizeof(String21) - 3), LIGHTGRAY, BLACK, 1);
//	LCD_Text(410, 270, String4, (sizeof(String21) - 3), LIGHTGRAY, BLACK, 1);
//	LCD_Text(410, 290, String4, (sizeof(String21) - 3), LIGHTGRAY, BLACK, 1);
//	LCD_Text(410, 310, String4, (sizeof(String21) - 3), LIGHTGRAY, BLACK, 1);
//	LCD_Text(410, 330, String4, (sizeof(String21) - 3), LIGHTGRAY, BLACK, 1);
//	LCD_Text(410, 350, String4, (sizeof(String21) - 3), LIGHTGRAY, BLACK, 1);
	j = 0;
	ImageCount = 0;

	res = f_opendir(&dir, path);
	if (res == FR_OK)
	{
		//LCD_Text(410, 210, String1, (sizeof(String1) - 1), YELLOW, BLACK, 1);
		while(HCD_IsDeviceConnected(&USB_OTG_Core))
		{
			res = f_readdir(&dir, &fno);
			if (res != FR_OK || fno.fname[0] == 0)
			{
				break;
			}
			if (fno.fname[0] == '.')
			{
				continue;
			}
#if _USE_LFN
			fn = *fno.lfname ? fno.lfname : fno.fname;
#else
			fn = fno.fname;
#endif
			/* Print Image File Names (*.bmp) to screen */
			for (i = 0; i < 7; i++)	// 30 is the MAX length of the text to fit Text window on screen
			{
				if (((fn[i] >= 0x2E) && (fn[i] <= 0x7A)) || (fn[i] == 0x20))		// '.' <= fn[i] <= 'z' || ' '
				{
					String50[i] = (uint8_t) fn[i];
				}
				else
				{
					String50[i] = 0x20;
				}
			}

			if((fno.fattrib & AM_MASK) == AM_DIR) {}
			else
			{
				for (i = 0; i < 60; i++)
				{
					if (((String50[i] == 'b') && (String50[i+1] == 'm') && (String50[i+2] == 'p')) ||
							((String50[i] == 'B') && (String50[i+1] == 'M') && (String50[i+2] == 'P')))
					{
						USART_SendString(PC, String50);
						USART_SendString(PC, "\r\n");
//						LCD_Text_J(410, 210 + j, String50, (sizeof(String50) - 1), WHITE, BLACK, 1);
						j += 20;
						ImageCount++;
					}
					else
					{
					}
				}
			}
	    }
	}
	else
	{
		USART_SendString(PC, String20);
//		LCD_Text(410, 210, String20, (sizeof(String20) - 3), BRIGHTRED, BLACK, 1);
	}

	F_ReadList = 0;
	USBH_USR_ApplicationState = USH_USR_FS_WAITCOMMAND;

	//set flag to write images
	F_ReadFile = 1;

	return res;
}

/**
* @brief  Image Browser
*         Browse Images on disk
* @param  path: pointer to root path
* @retval None
*/
uint8_t Image_Browser (char* path)
{
	int j = 0;
	FRESULT res;
	uint8_t ret = 1;
	FILINFO fno;
	DIR dir;
	char *fn;

#if _USE_LFN
	static char lfn[_MAX_LFN * (_DF1S ? 2 : 1) + 1];
	fno.lfname = lfn;
	fno.lfsize = sizeof(lfn);
#endif

	res = f_opendir(&dir, path);
	if (res == FR_OK)
	{
		for (;;)
		{
			res = f_readdir(&dir, &fno);
			if (res != FR_OK || fno.fname[0] == 0)
				break;
			if (fno.fname[0] == '.')
				continue;
#if _USE_LFN
			fn = *fno.lfname ? fno.lfname : fno.fname;
#else
			fn = fno.fname;
#endif
			if (fno.fattrib & AM_DIR)
			{
//				USART_SendString(PC, "Image Download Completed\r\n");
//				USART_SendString(PC, "Please remove Flash Drive now for System Re-boot...\r\n");
				F_WaitReboot = 1;
				continue;
			}
			else
			{
				for (j = 1; j <= 60; j++) //ImageCount
				{
					switch (j)
					{
					case 1:
						if((strstr(fn, "01.bmp")) || (strstr(fn, "01.BMP")))
						{
							USART_SendString(PC, "Downloading: 01.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 439, 0, 115, NOR_BLOCK_1);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 2:
						if((strstr(fn, "02.bmp")) || (strstr(fn, "02.BMP")))
						{
							USART_SendString(PC, "Downloading: 02.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 203, 0, 26, NOR_BLOCK_2);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 3:
						if((strstr(fn, "03.bmp")) || (strstr(fn, "03.BMP")))
						{
							USART_SendString(PC, "Downloading: 03.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 299, 0, 299, NOR_BLOCK_5);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 4:
						if((strstr(fn, "04.bmp")) || (strstr(fn, "04.BMP")))
						{
							USART_SendString(PC, "Downloading: 04.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 99, 0, 99, NOR_BLOCK_3);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 5:
						if((strstr(fn, "05.bmp")) || (strstr(fn, "05.BMP")))
						{
							USART_SendString(PC, "Downloading: 05.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 99, 0, 99, NOR_BLOCK_3a);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 6:
						if((strstr(fn, "06.bmp")) || (strstr(fn, "06.BMP")))
						{
							USART_SendString(PC, "Downloading: 06.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 99, 0, 99, NOR_BLOCK_4);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 7:
						if((strstr(fn, "07.bmp")) || (strstr(fn, "07.BMP")))
						{
							USART_SendString(PC, "Downloading: 07.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 99, 0, 99, NOR_BLOCK_4a);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 8:
						if((strstr(fn, "08.bmp")) || (strstr(fn, "08.BMP")))
						{
							USART_SendString(PC, "Downloading: 08.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 99, 0, 99, NOR_BLOCK_6a);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 9:
						if((strstr(fn, "09.bmp")) || (strstr(fn, "09.BMP")))
						{
							USART_SendString(PC, "Downloading: 09.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 99, 0, 99, NOR_BLOCK_7);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
							USART_SendString(PC, "Image Download Completed\r\n");
							USART_SendString(PC, "Please remove Flash Drive now for System Re-boot...\r\n");
						}
						break;
					case 54:
						if((strstr(fn, "54.bmp")) || (strstr(fn, "54.BMP")))
						{
							USART_SendString(PC, "Downloading: 54.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 397, 0, 469, NOR_BLOCK_115);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
							USART_SendString(PC, "Image Download Completed\r\n");
							USART_SendString(PC, "Please remove Flash Drive now for System Re-boot...\r\n");
						}
						break;
					case 55:
						if((strstr(fn, "55.bmp")) || (strstr(fn, "55.BMP")))
						{
							USART_SendString(PC, "Downloading: 55.bmp\r\n");
							res = f_open(&file, fn, FA_OPEN_EXISTING | FA_READ);
							Show_Image(0, 279, 0, 208, NOR_BLOCK_119);
							USB_OTG_BSP_mDelay(100);
							ret = 0;
							f_close(&file);
						}
						break;
					case 59:
						break;
					default:
						break;
					}
				}
			}
		}
	}

	USBH_USR_ApplicationState = USH_USR_FS_WAITCOMMAND;
	return ret;
}

/**
 * @brief  Show and Save Image
 *         Displays BMP image
 * @param  Address -> Block in NOR Flash to save image to
 * @retval None
 */
void Show_Image(uint16_t x0, uint16_t x1, uint16_t y0, uint16_t y1, uint32_t Address)
{
	uint16_t i = 0, j = 0;
	uint16_t numOfReadBytes = 0;
	FRESULT res;
	uint16_t dataBuf[IMAGE_BUFFER_SIZE/2] = {0};
	uint16_t dataBuf1[IMAGE_BUFFER_SIZE/2] = {0};
	uint32_t address = Address;
	uint16_t WRcount = 0;

	/* Bypass Bitmap header  - Bytes */
	f_lseek(&file, 66);//68); //54

	while (HCD_IsDeviceConnected(&USB_OTG_Core))
	{
		res = f_read(&file, Image_Buf, IMAGE_BUFFER_SIZE, (void *)&numOfReadBytes);

		if((numOfReadBytes == 0) || (res != FR_OK)) /*EOF or Error*/
		{
			NOR_ReturnToReadMode();
			address = Address;
			while (j-- > 0)
			{
				NOR_ReadBuffer(dataBuf1, address, IMAGE_BUFFER_SIZE / 2);
				for(i = 0 ; i < IMAGE_BUFFER_SIZE / 2; i++)
				{
//					LCD_WriteData(dataBuf1[i]);
				}
				address += (IMAGE_BUFFER_SIZE); 	//NOR
			}
			break;
		}

		for(i = 0 ; i < IMAGE_BUFFER_SIZE; i+= 2)
		{
//			LCD_WriteData(((uint16_t)Image_Buf[i+1] << 8) + ((uint16_t)Image_Buf[i]));
			dataBuf[i/2] = ((uint16_t)Image_Buf[i+1] << 8) + ((uint16_t)Image_Buf[i]);
		}

		NOR_WriteBuffer(dataBuf, address, IMAGE_BUFFER_SIZE/2);
		address += (IMAGE_BUFFER_SIZE); 	//NOR
		j++;
		WRcount++;
	}
}

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/






























