#include "usb.h"
#include "ff.h"
#include "ff_gen_drv.h"
#include "Src/ESAL/usb/usbh_diskio_dma.h"
#include "Src/ESAL/uart/uart.h"
#include "Src/ESAL/norflash/M29W128.h"
#include "Src/ESAL/lcd/lcd.h"

USB_tenDiskConnectState		USB_DiskConnectState;
USB_tenStates				USB_States;
char						USBDISKPath[4]; /* USB Host logical drive path */
FATFS						USBH_fatfs;
FIL							MyFile;
FRESULT						res;
uint32_t					u32bytesWritten;
uint8_t						u8ReadText[200];
uint8_t						u8WriteText[] = "USB Host Library : Mass Storage Example";
uint8_t						u8ImageBuffer[IMAGE_BUFFER_SIZE];

extern USBH_HandleTypeDef	hUSBHost;

void USB_vInit(void)
{
	USB_DiskConnectState = USB_DRIVE_IDLE;
	USB_States = USB_STATE_IDLE;
	/* Init Host Library */
	USBH_Init(&hUSBHost, USBH_UserProcess, 0);
	/* Add Supported Class */
	USBH_RegisterClass(&hUSBHost, USBH_MSC_CLASS);
	/* Start Host Process */
	USBH_Start(&hUSBHost);
}

/**
  * @brief  User Process
  * @param  phost: Host Handle
  * @param  id: Host Library user message ID
  * @retval None
  */
void USBH_UserProcess(USBH_HandleTypeDef * phost, uint8_t id)
{
	switch (id)
	{
	case HOST_USER_SELECT_CONFIGURATION:
		{
			
		}
		break;
	case HOST_USER_DISCONNECTION:
		{
			USB_DiskConnectState = USB_DRIVE_DISCONNECTED;
			USB_enSetState(USB_STATE_IDLE);
			if (FATFS_UnLinkDriver(USBDISKPath) == 0)
			{
				if (f_mount(NULL, "", 0) != FR_OK)
				{
					USART3_vDebugPrint("[FATFS] ERROR: Cannot DeInitialize FatFs!\r\n");
				}
			}
			/* Reboot CPU */
			HAL_NVIC_SystemReset();
		}
		break;
	case HOST_USER_CLASS_ACTIVE:
		{
			USB_DiskConnectState = USB_DRIVE_CONNECTED;
		}
		break;
	case HOST_USER_CONNECTION:
		{
			if (FATFS_LinkDriver(&USBH_Driver, USBDISKPath) == 0)
			{
				if (f_mount(&USBH_fatfs, "", 0) != FR_OK)
				{
					USART3_vDebugPrint("[FATFS] ERROR: Cannot Initialize FatFs!\r\n");
				}
			}
		}
		break;
	default:
		break;
	}
}

void USB_vMain(void)
{
	uint16_t u16bytesRead;
	
	switch (USB_States)
	{
	case USB_STATE_IDLE:
		{
			if (USB_DiskConnectState == USB_DRIVE_CONNECTED)
			{
				USB_enSetState(USB_STATE_EXPLORER);
			}
		}
		break;
	case USB_STATE_EXPLORER:
		{
			/* Display Disk Contents */
			USART3_vDebugPrint("[USB] Exploring Disk Contents...\r\n");
			USB_vExploreDisk("0:/", 1);
			USB_enSetState(USB_STATE_SAVE_IMAGES);
		}
		break;
	case USB_STATE_SAVE_IMAGES:
		{
			USB_vImageBrowser("0:/");
			USB_enSetState(USB_STATE_WAIT);
		}
		break;
	case USB_STATE_WAIT:
		{
			
		}
		break;
	case USB_WRITE_TEXT_FILE:
		{
			USB_vWriteTextFile(&MyFile, "0:USBHost.txt", u8WriteText, sizeof(u8WriteText), (void *)&u32bytesWritten);
			USB_enSetState(USB_READ_TEXT_FILE);
		}
		break;
	case USB_READ_TEXT_FILE:
		{
			USB_vReadTextFile(&MyFile, "0:USBHost.txt", u8ReadText, sizeof(u8ReadText), (void *)&u16bytesRead);
			USB_enSetState(USB_STATE_WAIT);
		}
		break;
	default:
		break;
	}
}

USB_tenStates USB_enGetState(void)
{
	return USB_States;
}

void USB_enSetState(USB_tenStates state)
{
	USB_States = state;
}

FRESULT USB_vExploreDisk(char *path, uint8_t recu_level)
{
	FRESULT res = FR_OK;
	FILINFO fno;
	DIR dir;
	char *fn;
	char tmp[14];
	uint8_t line_idx = 0;

	res = f_opendir(&dir, path);
	if (res == FR_OK)
	{
		while (USBH_MSC_IsReady(&hUSBHost))
		{
			res = f_readdir(&dir, &fno);
			if (res != FR_OK || fno.fname[0] == 0)
			{
				break;
			}
			if (fno.fname[0] == '.')
			{
				continue;
			}
 
			fn = fno.fname;
			strcpy(tmp, fn);

			line_idx++;
			if (line_idx > 9)
			{
				line_idx = 0;
			}

			if (recu_level == 1)
			{
				USART3_vDebugPrint("   |__");
			}
			else if (recu_level == 2)
			{
				USART3_vDebugPrint("   |   |__");
			}
			
			if ((fno.fattrib & AM_DIR) == AM_DIR)
			{
				strcat(tmp, "\r\n");
				USART3_vDebugPrint((void *)tmp);
				USB_vExploreDisk(fn, 2);
			}
			else
			{
				strcat(tmp, "\r\n");
				USART3_vDebugPrint((void *)tmp);
			}

			if (((fno.fattrib & AM_DIR) == AM_DIR) && (recu_level == 2))
			{
				USB_vExploreDisk(fn, 2);
			}
		}
		f_closedir(&dir);
	}
	return res;
}

FRESULT USB_vImageBrowser(char * path)
{
	FRESULT res = FR_OK;
	FILINFO fno;
	DIR dir;
	char *fn;
	char tmp[14];
	uint16_t u16i = 0;
	uint16_t u16j = 0;
	uint16_t numOfReadBytes = 0;
	uint32_t ImageAddress = 0;
	uint16_t dataBuf[IMAGE_BUFFER_SIZE / 2] = { 0 };
	
	res = f_opendir(&dir, path);
	if (res == FR_OK)
	{
		while (USBH_MSC_IsReady(&hUSBHost))
		{
			res = f_readdir(&dir, &fno);
			if (res != FR_OK || fno.fname[0] == 0)
			{
				break;
			}
			if (fno.fname[0] == '.')
			{
				continue;
			}
 
			fn = fno.fname;
			strcpy(tmp, fn);
			
			if (fno.fattrib & AM_DIR)
			{
				continue;
			}
			else
			{
				/**************************/
				/***** Jetcharge LOGO *****/
				/**************************/
				if ((strstr(fn, "01.bmp")) || (strstr(fn, "01.BMP")))
				{
					USART3_vDebugPrint("[USB] Downloading 01.bmp...\r\n");
					ImageAddress = NOR_BLOCK_0;
					/* Erase the BLOCK in NOR Flash */
					NOR_u8EraseBlock(NOR_BLOCK_0, Bank1_NOR1_ADDR);
					NOR_u8EraseBlock(NOR_BLOCK_1, Bank1_NOR1_ADDR);
//					NOR_u8EraseBlock(ImageAddress, Bank1_NOR1_ADDR);
//					NOR_u8EraseBlock(ImageAddress, Bank1_NOR1_ADDR);
					/* Open the Image file */
					res = f_open(&MyFile, fn, FA_OPEN_EXISTING | FA_READ);
					/* Move the file pointer past the BMP header */
					f_lseek(&MyFile, 66);
					while (USBH_MSC_IsReady(&hUSBHost))
					{
						/* Read Image into buffer */
						res = f_read(&MyFile, u8ImageBuffer, IMAGE_BUFFER_SIZE, (void *)&numOfReadBytes);
						/* Save Image to NOR Flash */
						if ((numOfReadBytes == 0) || (res != FR_OK)) /*EOF or Error*/
						{
							ImageAddress = NOR_BLOCK_0;
							HAL_NOR_ReturnToReadMode(&hnor1);
							/* Display Image on LCD */
							LCD_vClearScreen(BLACK);
							LCD_vPrintText(0, LINE1, "01.bmp", sizeof("01.bmp") - 1, WHITE, BLACK, MEDIUM_FONT);
							LCD_vDrawBitmap(52, 160, imJetChargeLogo);
							USART3_vDebugPrint("[USB] Image 01.bmp saved to NOR Flash...\r\n");
							break;
						}
						else
						{
							/* Write data to NOR Flash */
							for (u16i = 0; u16i < IMAGE_BUFFER_SIZE; u16i += 2)
							{
								dataBuf[u16i / 2] = ((uint16_t) u8ImageBuffer[u16i + 1] << 8) + ((uint16_t) u8ImageBuffer[u16i]);
							}
							NOR_u8WriteBuffer(dataBuf, ImageAddress, IMAGE_BUFFER_SIZE / 2);
							ImageAddress += (IMAGE_BUFFER_SIZE);
							u16j++;
						}
					}
					f_close(&MyFile);
				}
			}
		}
	}
	
	return res;
}
FRESULT USB_vWriteTextFile(FIL * fp, char *path, const void * buff, uint32_t bytestowrite, uint32_t * byteswritten)
{
	FRESULT res = FR_OK;
	
	res = f_open(fp, path, FA_CREATE_ALWAYS | FA_WRITE);
	if (res != FR_OK)
	{
		USART3_vDebugPrint("Cannot Open file\r\n");
	}
	else
	{
		USART3_vDebugPrint("[USB] File opened for write\r\n");
		res = f_write(fp, buff, bytestowrite, (void *) &byteswritten);
		if ((byteswritten == 0) || (res != FR_OK))  /* EOF or Error */
		{
			USART3_vDebugPrint("[USB] Cannot Write on the file\r\n");
		}
		else
		{
			USART3_vDebugPrint("[USB] File write complete\r\n");
		}
		/* Close file */
		f_close(fp);
	}
	
	return res;
}
	
FRESULT USB_vReadTextFile(FIL * fp, char *path, void * buff, uint32_t bytestoread, uint32_t * bytesread)
{
	FRESULT res = FR_OK;
	
	res = f_open(fp, path, FA_READ);
	if (res != FR_OK)
	{
		USART3_vDebugPrint("[USB] Cannot Open file for read\r\n");
	}
	else
	{	
		USART3_vDebugPrint("[USB] Reading file\r\n");

		res = f_read(fp, buff, bytestoread, (void *)&bytesread);

		if ((bytesread == 0) || (res != FR_OK)) /* EOF or Error */
		{
			USART3_vDebugPrint("[USB] Cannot Read from the file\r\n");
		}
		else
		{
			USART3_vDebugPrint("Read Text : \n");
			USART3_vDebugPrint((char *)buff);
			USART3_vDebugPrint("\r\n");
		}
		f_close(&MyFile);
	}		
	
	return res;
}
	
