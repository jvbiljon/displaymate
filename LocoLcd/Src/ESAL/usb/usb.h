/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_H
#define __USB_H

#include "main.h"
#include "stm32f4xx_hal.h"
#include "ff.h"
#include "ff_gen_drv.h"
#include "usbh_diskio_dma.h"

#define IMAGE_BUFFER_SIZE	512

typedef enum
{
	USB_DRIVE_IDLE = 0,
	USB_DRIVE_CONNECTED,
	USB_DRIVE_DISCONNECTED
} USB_tenDiskConnectState;
void USB_vInit(void);
void USBH_UserProcess(USBH_HandleTypeDef *, uint8_t);

typedef enum
{
	USB_STATE_IDLE = 0,
	USB_STATE_EXPLORER,
	USB_STATE_SAVE_IMAGES,
	USB_STATE_WAIT,
	USB_WRITE_TEXT_FILE,
	USB_READ_TEXT_FILE
} USB_tenStates;
void USB_vMain(void);
USB_tenStates USB_enGetState(void);
void USB_enSetState(USB_tenStates);

FRESULT USB_vExploreDisk(char *, uint8_t);
FRESULT USB_vImageBrowser(char *);
FRESULT USB_vWriteTextFile(FIL *, char *, const void *, uint32_t, uint32_t *);
FRESULT USB_vReadTextFile(FIL *, char *, void *, uint32_t, uint32_t *);

#endif /* __USB_H */
