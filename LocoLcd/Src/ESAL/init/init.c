#include "Src/ESAL/init/init.h"
#include "Src/ESAL/fsmc/fsmc.h"
#include "Src/ESAL/lcd/lcd.h"
#include "Src/ESAL/uart/uart.h"
#include "Src/ESAL/usb/usb.h"

extern uint8_t	USART3TEMPBUFF[];

void INIT_vCpu(void)
{
	/* Configure the system clock */
	SystemClock_Config();
	/* Configure GPIO's */
	INIT_vGpioConfig();
	/* Configure USART 3 */
	INIT_vUsart3Config(); 
	/* Configure USB Mass Storage Device Class */
	INIT_vUsbMscConfig();
	/* Configure FSMC */
	INIT_vFsmcConfig();
	/* Configure 7" LCD */
	INIT_vConfigLCD();
}

/*
 * System Clock Configuration
*/
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

	/* Configure the main internal regulator output voltage */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/* Initializes the CPU, AHB and APB busses clocks */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 15;
	RCC_OscInitStruct.PLL.PLLN = 144;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 5;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/* Initializes the CPU, AHB and APB busses clocks */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
	                            | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
	PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/* Configure the Systick interrupt time */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/* Configure the Systick */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/*
 * Real-Time Clock Configuration
 **/
void INIT_vRtcConfig(void)
{
	__HAL_RCC_RTC_ENABLE();
	
	HAL_RtcHandle.Instance = RTC;
	HAL_RtcHandle.Init.HourFormat = RTC_HOURFORMAT_24;
	HAL_RtcHandle.Init.AsynchPrediv = 0x7F;
	HAL_RtcHandle.Init.SynchPrediv = 0x00FF;
	HAL_RtcHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
	HAL_RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	HAL_RtcHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
	if (HAL_RTC_Init(&HAL_RtcHandle) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
	
	/* Initialize RTC and set the Time and Date */	
	if (HAL_RTCEx_BKUPRead(&HAL_RtcHandle, RTC_BKP_DR0) != 0x32F2) 
	{
		HAL_sTime.Hours = 16;
		HAL_sTime.Minutes = 30;
		HAL_sTime.Seconds = 0;
		HAL_sTime.SubSeconds = 0;
		HAL_sTime.TimeFormat = RTC_HOURFORMAT_24;
		HAL_sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
		HAL_sTime.StoreOperation = RTC_STOREOPERATION_RESET;
		if (HAL_RTC_SetTime(&HAL_RtcHandle, &HAL_sTime, RTC_FORMAT_BIN) != HAL_OK)
		{
			_Error_Handler(__FILE__, __LINE__);
		}

		HAL_sDate.WeekDay = RTC_WEEKDAY_MONDAY;
		HAL_sDate.Month = RTC_MONTH_JANUARY;
		HAL_sDate.Date = 15;
		HAL_sDate.Year = 18;
		if (HAL_RTC_SetDate(&HAL_RtcHandle, &HAL_sDate, RTC_FORMAT_BIN) != HAL_OK)
		{
			_Error_Handler(__FILE__, __LINE__);
		}

		HAL_RTCEx_BKUPWrite(&HAL_RtcHandle, RTC_BKP_DR0, 0x32F2);
	}
}

void INIT_vGpioConfig(void)
{
	/* Enable GPIO Clocks */
	__GPIOA_CLK_ENABLE();
	__GPIOB_CLK_ENABLE();
	__GPIOC_CLK_ENABLE();
	__GPIOD_CLK_ENABLE();
	__GPIOE_CLK_ENABLE();
	__GPIOF_CLK_ENABLE();
	__GPIOG_CLK_ENABLE();
	__GPIOH_CLK_ENABLE();
	
	/************ LED's ***********/
	/* Select GPIO Pins to set up */
	GPIO_InitStructure.Pin = GPIO_PIN_6 | GPIO_PIN_7;				
	/* Select Pin Setup Configuration */
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStructure);	
	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7 | GPIO_PIN_7, GPIO_PIN_RESET);
	
	/************ RS485 ***********/
	GPIO_InitStructure.Pin = GPIO_PIN_0;				
	/* Select Pin Setup Configuration */
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStructure);	
	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_0, GPIO_PIN_RESET);

	/************* USB ************/
	/* Select GPIO Pins to set up */
	GPIO_InitStructure.Pin = GPIO_PIN_8 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12;				
	/* Select Pin Setup Configuration */
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF10_OTG_FS;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);	

	/* Select GPIO Pins to set up */
	GPIO_InitStructure.Pin = GPIO_PIN_9;
	/* Select Pin Setup Configuration */
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void INIT_vUsart3Config(void)
{
	/* Enable Clock */
	__USART3_CLK_ENABLE();
	/* Setup GPIO for Alternate Functions */
	/* RX|TX Pins */
	GPIO_InitStructure.Pin = GPIO_PIN_10 | GPIO_PIN_11;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF7_USART3;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
	/* Setup Interrupt */
	HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(USART3_IRQn);
	/* Setup USART Paramters */
	HAL_Uart3.Instance = USART3;
	HAL_Uart3.Init.BaudRate = 115200;
	HAL_Uart3.Init.WordLength = UART_WORDLENGTH_8B;
	HAL_Uart3.Init.StopBits = UART_STOPBITS_1;
	HAL_Uart3.Init.Parity = USART_PARITY_NONE;
	HAL_Uart3.Init.Mode = UART_MODE_TX_RX;
	HAL_Uart3.Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
	HAL_Uart3.Init.OverSampling = UART_OVERSAMPLING_8;
	
	if (HAL_UART_Init(&HAL_Uart3) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
	/* Clear the Buffer */
	USART3_vClearRxBuffer();
	USART3_vClearTxBuffer();
	USART3_vClearRxIndex();
	/* Enable the RX Interrupt to receive 1-byte */
	HAL_UART_Receive_IT(&HAL_Uart3, (uint8_t *)USART3TEMPBUFF, 1);
}

void INIT_vUsbMscConfig(void)
{
	USB_vInit();
}

void INIT_vFsmcConfig(void)
{
	FSMC_vInit();
}

void INIT_vConfigLCD(void)
{
	HAL_Delay(1000);
	LCD_vInit();
}

void _Error_Handler(char * file, int line)
{
	/* Initialisation Error Handler */	
	while (1) 
	{
		
	}
}

