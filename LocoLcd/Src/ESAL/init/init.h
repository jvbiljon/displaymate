/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __INIT_H
#define __INIT_H

#include "main.h"
#include "stm32f4xx_hal.h"

void INIT_vCpu(void);
void SystemClock_Config(void);
void INIT_vRtcConfig(void);
void INIT_vGpioConfig(void);
void INIT_vUsart3Config(void);
void INIT_vUsbMscConfig(void);
void INIT_vFsmcConfig(void);
void INIT_vConfigLCD(void);
void _Error_Handler(char * , int);

#endif /* __INIT_H */
