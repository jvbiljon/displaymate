#include "M29W128.h"

NOR_IDTypeDef NOR_stDeviceInfo;

void NOR_vReadDeviceId(void)
{
	HAL_NOR_Read_ID(&hnor1, &NOR_stDeviceInfo);
}

uint8_t NOR_u8EraseBlock(uint32_t BlockAddress, uint32_t Address)
{
	HAL_NOR_Erase_Block(&hnor1, BlockAddress, Address);

	return HAL_NOR_GetStatus(&hnor1, Bank1_NOR1_ADDR, NOR_Timeout);
}

uint8_t NOR_u8FillBuffer(uint16_t *pBuffer, uint16_t BufferLength, uint32_t Offset)
{
	uint16_t u16i = 0;

	for (u16i = 0; u16i < BufferLength; u16i++)
	{
		pBuffer[u16i] = u16i + Offset;
	}
	return DONE;
}

uint8_t NOR_WriteHalfWord(uint32_t WriteAddr, uint16_t Data)
{
	NOR_WRITE(NOR_ADDR_SHIFT(Bank1_NOR1_ADDR, NOR_MEMORY_16B, 0x0555), 0x00AA);
	NOR_WRITE(NOR_ADDR_SHIFT(Bank1_NOR1_ADDR, NOR_MEMORY_16B, 0x02AA), 0x0055);
	NOR_WRITE(NOR_ADDR_SHIFT(Bank1_NOR1_ADDR, NOR_MEMORY_16B, 0x0555), 0x00A0);
	NOR_WRITE((Bank1_NOR1_ADDR + WriteAddr), Data);

	return HAL_NOR_GetStatus(&hnor1, Bank1_NOR1_ADDR, NOR_Timeout);
}

uint8_t NOR_u8WriteBuffer(uint16_t* pBuffer, uint32_t WriteAddr, uint32_t BufferSize)
{
	uint8_t res = NOR_ONGOING;
	uint32_t NumHalfwordToWrite = BufferSize;
	
	do
	{
		/*!< Transfer data to the memory */
		res = NOR_WriteHalfWord(WriteAddr, *pBuffer++);
		WriteAddr += 2;
		NumHalfwordToWrite--;
	} while ((res == NOR_SUCCESS) && (NumHalfwordToWrite != 0));

	return HAL_NOR_GetStatus(&hnor1, Bank1_NOR1_ADDR, NOR_Timeout);
}

uint8_t NOR_u8ReadBuffer(uint16_t *pBuffer, uint32_t ReadAddr, uint32_t BufferSize)
{
//	HAL_NOR_ReadBuffer(&hnor1, ReadAddr, pBuffer, BufferSize);
	
	uint32_t NumHalfwordToRead = BufferSize;

	NOR_WRITE(NOR_ADDR_SHIFT(Bank1_NOR1_ADDR, NOR_MEMORY_16B, 0x0555), 0x00AA);
	NOR_WRITE(NOR_ADDR_SHIFT(Bank1_NOR1_ADDR, NOR_MEMORY_16B, 0x02AA), 0x0055);
	NOR_WRITE((Bank1_NOR1_ADDR + ReadAddr), 0x00F0);

	for (; NumHalfwordToRead != 0x00; NumHalfwordToRead--) /*!< while there is data to read */
	{
		/*!< Read a Halfword from the NOR */
		*pBuffer++ = *(__IO uint16_t *)((Bank1_NOR1_ADDR + ReadAddr));
		ReadAddr += 2;
	}

	return HAL_NOR_GetStatus(&hnor1, Bank1_NOR1_ADDR, NOR_Timeout);
}

uint8_t NOR_u8Test(void)
{
	uint8_t res = WAIT;
	uint8_t result = 0;
	uint16_t index = 0;
	
	/* Read NOR Flash Device ID */
	NOR_vReadDeviceId();
	/* Return Device to READ mode */
	HAL_NOR_ReturnToReadMode(&hnor1);
	/* Erase Memory Block */
	res = NOR_u8EraseBlock(NOR_BLOCK_127, Bank1_NOR1_ADDR);
	/* Fill buffer with test data */
	res = NOR_u8FillBuffer(NORTxBuffer, NOR_BUFFER_SIZE, 0x3210);
	/* Write data to NOR Flash */
	res = NOR_u8WriteBuffer(NORTxBuffer, NOR_BLOCK_127, NOR_BUFFER_SIZE);
	/* Read data just written into Flash */
	res = NOR_u8ReadBuffer(NORRxBuffer, NOR_BLOCK_127, NOR_BUFFER_SIZE);
	/* Now compare RX & TX buffers */
	for (index = 0; index < (NOR_BUFFER_SIZE - 1); index++)
	{
		if (NORTxBuffer[index] == NORRxBuffer[index])
		{
			result = PASS;
		}
		else
		{
			return FAIL;
		}
	}
	
	return result;
}