#include "state.h"
#include "Src/ESAL/init/init.h"
#include "Src/ESAL/norflash/M29W128.h"
#include "Src/ESAL/lcd/lcd.h"
#include "Src/ESAL/uart/uart.h"
#include "Src/ESAL/usb/usb.h"

static STATE_tenSystemStates	STATE__enCurrentState;

void STATE_vInit(void)
{
	STATE__enCurrentState = STATE_nReset;
}

void STATE_vMain(void)
{	
	switch (STATE__enCurrentState)
	{
	case STATE_nReset:
		{
			HAL_Init();		
			STATE__enCurrentState = STATE_nInit;
		}
		break;

	case STATE_nInit:
		{
			INIT_vCpu();
			USART3_vDebugPrint("LCD Development on Mining PCB\r\n");
			/* Perform test on the NOR Flash */
			NOR_u8Test();
			/* Show LCD HomeScreen */
			LCD_vDisplayHomescreen();

			STATE__enCurrentState = STATE_nRun;	
		}	
		break;

	case STATE_nRun:
		{
			/* USB Host Background task */
			USBH_Process(&hUSBHost);
			/* Run USB Main Application Tasks */
			USB_vMain();
		}
		break;

	case STATE_nShutdown:
		{
		}
		break;

	case STATE_nSleep:
		{
		}
		break;

	case STATE_nFailure:
		{
		}
		break;
            
	default:
		break;
	}
}

STATE_tenSystemStates STATE_enGetState(void)
{
	return STATE__enCurrentState;
}

void STATE_enSetState(STATE_tenSystemStates STATE__enNewState)
{
	STATE__enCurrentState = STATE__enNewState;
}

