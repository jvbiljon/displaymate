/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include <stm32f4xx_hal.h>
#include <stm32_hal_legacy.h>
#include <stdio.h>
#include <stdlib.h>
#include "usbh_msc.h" 

#define	FALSE				0
#define TRUE				1

#define WAIT				0
#define PASS				1
#define FAIL				2
#define BUSY				3
#define DONE				4

/* Exported types ------------------------------------------------------------*/
RCC_ClkInitTypeDef		RCC_ClkInitStruct;
RCC_OscInitTypeDef		RCC_OscInitStruct;

RTC_HandleTypeDef		HAL_RtcHandle;
RTC_TimeTypeDef			HAL_sTime;
RTC_DateTypeDef			HAL_sDate;

GPIO_InitTypeDef		GPIO_InitStructure;

TIM_OC_InitTypeDef		TIM1_InitStructure;
TIM_HandleTypeDef		HAL_TIM1;

UART_HandleTypeDef		HAL_Uart3;
UART_HandleTypeDef		HAL_Uart5;

ADC_HandleTypeDef		HAL_ADC1;
ADC_HandleTypeDef		HAL_ADC2;
ADC_ChannelConfTypeDef	AdcChannel;

DMA_HandleTypeDef		g_DmaHandle;

NOR_HandleTypeDef		hnor1;
SRAM_HandleTypeDef		hsram2;

HCD_HandleTypeDef		hhcd;
USBH_HandleTypeDef		hUSBHost;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

